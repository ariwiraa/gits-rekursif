package main

import (
	"fmt"
	"math"
)

func DeretBilangan(i float32, n float32, input float32) float32 {
	var total float32 = 0.0
	if n < input {
		bagi := i / n
		total += bagi
		fmt.Printf("%.2f + ", bagi)
		return bagi + DeretBilangan(i, n+1, input)
	} else {
		bagi := i / n
		total += bagi
		fmt.Printf("%.2f ", bagi)
		return bagi
	}
}

func deretBerpangkat(n int, input int, posisi string) string {
	if n < input {
		pangkat := int(math.Pow(float64(n), float64(2)))
		if posisi == "atas" {
			fmt.Printf("%dx/%d + ", pangkat, n)
		} else {
			fmt.Printf("%d/%dx + ", n, pangkat)
		}
		return deretBerpangkat(n+1, input, posisi)
	} else {
		pangkat := int(math.Pow(float64(n), float64(2)))
		if posisi == "atas" {
			fmt.Printf("%dx/%d ", pangkat, n)
		} else {
			fmt.Printf("%d/%dx ", n, pangkat)
		}
		return ""
	}
}

func main() {
	var input int
	fmt.Print("Masukan panjang deret :")
	fmt.Scan(&input)
	fmt.Print("a. SUM = ")
	fmt.Println("=", DeretBilangan(20, 1, float32(input)))
	fmt.Print("b. SUM = ")
	fmt.Println("=", DeretBilangan(100, 1, float32(input)))
	fmt.Print("c. SUM = ")
	fmt.Println("=", DeretBilangan(1, 1, float32(input)))
	fmt.Print("d. SUM = ")
	fmt.Println("=", deretBerpangkat(1, input, "bawah"))
	fmt.Print("e. SUM = ")
	fmt.Println("=", deretBerpangkat(1, input, "atas"))

}
