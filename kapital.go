package main

import (
	"fmt"
	"unicode"
)

func CheckCapital(word string, n int) int {
	if n < len(word) {
		y := rune(word[n])
		if !unicode.IsUpper(y) {

			return 1 + CheckCapital(word, n+1)
		} else {
			return 0 + CheckCapital(word, n+1)
		}
	}
	return 0
}

func CheckNumber(word string, n int) int {
	if n < len(word) {
		y := rune(word[n])
		if !unicode.IsNumber(y) {

			return 1 + CheckNumber(word, n+1)
		} else {
			return 0 + CheckNumber(word, n+1)
		}
	}
	return 0
}
func main() {
	word := "Makan"
	fmt.Printf("Jumlah kata non kapital dari '%s' : ", word)
	fmt.Println(CheckCapital(word, 0))

	wordWithNumber := "JKT48"
	fmt.Printf("Jumlah kata yang ada angkanya dari '%s' : ", wordWithNumber)
	fmt.Println(CheckCapital(wordWithNumber, 0))

}
